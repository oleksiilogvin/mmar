from typing import Final

import numpy as np
import pandas as pd
import statsmodels.api as sm
import multiprocessing as mp
from fbm import FBM
from scipy.optimize import fsolve

# TODO: Are these moments optimal
STAT_MOMENTS_Q: Final = np.array(
    [0.01, 0.1, 0.2, 0.3, 0.4, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1.0, 1.05, 1.1, 1.15,
     1.2, 1.25, 1.3, 1.35, 1.4, 1.45, 1.5, 1.55, 1.6, 1.65, 1.7, 1.75, 1.8, 1.81, 1.82, 1.83, 1.84, 1.85,
     1.86, 1.87, 1.88, 1.89, 1.9, 1.91, 1.92, 1.93, 1.94, 1.95, 1.96, 1.97, 1.98, 1.985, 1.99, 1.991,
     1.992, 1.993, 1.994, 1.995, 1.996, 1.997, 1.998, 1.999, 2.0, 2.001, 2.002, 2.003, 2.004, 2.005, 2.006,
     2.007, 2.008, 2.009, 2.01, 2.015, 2.02, 2.025, 2.03, 2.04, 2.05, 2.06, 2.07, 2.08, 2.09, 2.1, 2.15,
     2.2, 2.25, 2.3, 2.35, 2.4, 2.45, 2.5, 2.6, 2.7, 2.8, 2.9, 3.0, 3.1, 3.2, 3.3, 3.4, 3.5, 3.6, 3.7, 3.8,
     3.9, 4.0, 4.5])


# TODO: Try to get rid of loops - https://github.com/JeffPaine/beautiful_idiomatic_python#looping-over-a-collection
async def get_result(prices: np.ndarray) -> np.ndarray:
    highly_composite_number = prices.size - 1
    price_log_deltas_xt = np.log(prices) - np.log(prices[0])
    time_deltas = get_time_deltas(highly_composite_number, prices)

    sample_sums = estimate_normalized_partition_function(time_deltas, price_log_deltas_xt)
    tau_q = estimate_scaling_function_tau_q(sample_sums, time_deltas, highly_composite_number)
    tau_q_polyfit = get_polyfit(tau_q)

    b = 2

    a0 = get_holder_exponent_a(tau_q_polyfit)
    h = get_hurst_exponent_h(tau_q_polyfit)
    mean_lambda = a0 / h
    sigma = (2 * (mean_lambda - 1)) / np.log(b)

    return get_mmar_simulation_samples(12, h, mean_lambda, sigma, 10)


def get_polyfit(tau_q: np.ndarray) -> np.ndarray:
    return np.polyfit(STAT_MOMENTS_Q, tau_q, 2)


def get_time_deltas(highly_composite_number: int, prices: np.ndarray) -> np.ndarray:
    time_points = np.arange(1, prices.size)
    deltas_filter = highly_composite_number % time_points == 0
    time_deltas = time_points[deltas_filter]
    return time_deltas


def estimate_normalized_partition_function(time_deltas: np.ndarray, price_log_deltas_xt: np.ndarray) -> np.ndarray:
    sample_sums = np.zeros((STAT_MOMENTS_Q.size, time_deltas.size))
    for q in range(0, STAT_MOMENTS_Q.size):
        for d in range(0, time_deltas.size):
            i = 0
            while i < (price_log_deltas_xt.size / time_deltas[d]) - 1:
                previous_price_index = i * time_deltas[d]
                next_price_index = previous_price_index + time_deltas[d]
                prices_delta = price_log_deltas_xt[next_price_index] - price_log_deltas_xt[previous_price_index]
                sample_sums[q, d] += abs(prices_delta) ** STAT_MOMENTS_Q[q]
                i += 1
        # TODO: Why is normalization needed?
        sample_sums[q] = np.log(sample_sums[q] / sample_sums[q, 0])
    return sample_sums


def estimate_scaling_function_tau_q(sample_sums: np.ndarray,
                                    time_deltas: np.ndarray,
                                    highly_composite_number: int) -> np.ndarray:
    tau_q = np.zeros(STAT_MOMENTS_Q.size)
    time_deltas_logs = np.log(time_deltas)

    # TODO: investigate why df is needed
    tau_regression = {'LN_DELTA': time_deltas_logs,
                      'LN_T': [np.log(highly_composite_number) for _ in range(time_deltas_logs.size)]}
    df = pd.DataFrame(tau_regression)

    for i in range(0, STAT_MOMENTS_Q.size):
        tau_q[i] = sm.OLS(sample_sums[i], df[['LN_DELTA', 'LN_T']], 'drop').fit().params[0]
    return tau_q


def get_holder_exponent_a(tau_q_polyfit: np.ndarray) -> np.ndarray:
    f_a = np.zeros(STAT_MOMENTS_Q.size)
    p = np.zeros(STAT_MOMENTS_Q.size)

    a = tau_q_polyfit[0]
    b = tau_q_polyfit[1]
    c = tau_q_polyfit[2]

    for i in range(0, STAT_MOMENTS_Q.size):
        p[i] = 2 * a * STAT_MOMENTS_Q[i] + b
        x = (p[i] - b) / (2 * a)
        f_a[i] = x * p[i] - (a * x * x + b * x + c)

    # TODO: How to use MF spectrum then?
    return p[0]


def get_hurst_exponent_h(tau_q_polyfit: np.ndarray) -> float:
    a = tau_q_polyfit[0]
    b = tau_q_polyfit[1]
    c = tau_q_polyfit[2]

    def f(x):
        return a * x ** 2 + b * x + c

    roots = fsolve(f, np.array([0, 4]))
    return 1 / roots[0]


def estimate_lognormal_cascade(k: int, v, mean_lambda: float, sigma: float) -> np.ndarray:
    k = k - 1

    if k >= 0:
        m0 = np.random.lognormal(mean_lambda, sigma)
        m1 = np.random.lognormal(mean_lambda, sigma)
        d = np.array([estimate_lognormal_cascade(k, m0 * v, mean_lambda, sigma),
                      estimate_lognormal_cascade(k, m1 * v, mean_lambda, sigma)]).flatten()
        v = d

    return v


def get_trading_time(simulation_range: int, lognormal_cascade: np.ndarray) -> np.ndarray:
    return simulation_range * np.cumsum(lognormal_cascade) / sum(lognormal_cascade)


# TODO: think/read about correct length param
def get_fbm_simulation(simulation_range: int, h: float) -> np.ndarray:
    fbm_instance = FBM(n=10 * simulation_range + 1, hurst=h, length=1, method='daviesharte')
    return fbm_instance.fbm()


def simulate_price_logs(fbm_simulation: np.ndarray, trading_time: np.ndarray) -> np.ndarray:
    return np.array([fbm_simulation[int(t * 10)] for t in trading_time])


def get_mmar_simulation(k: int, h: float, mean_lambda: float, sigma: float) -> np.ndarray:
    simulation_range = 2 ** k
    lognormal_cascade = estimate_lognormal_cascade(k, 1, mean_lambda, sigma)
    trading_time = get_trading_time(simulation_range, lognormal_cascade)
    fbm_simulation = get_fbm_simulation(simulation_range + 1, h)
    simulated_price_logs = simulate_price_logs(fbm_simulation, trading_time)
    return simulated_price_logs


def get_mmar_simulation_samples(k: int, h: float, mean_lambda: float, sigma: float, number: int) -> np.ndarray:
    pool = mp.Pool(mp.cpu_count())
    simulations = np.array(
        [pool.apply_async(get_mmar_simulation, (k, h, mean_lambda, sigma)) for _ in range(0, number)])
    simulations = np.array([s.get() for s in simulations])
    pool.close()
    return simulations

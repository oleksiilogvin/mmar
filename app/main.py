import numpy as np
from fastapi import FastAPI, Body
from pydantic import BaseModel

from app import hurst_exponent, mmar


class PricesRequest(BaseModel):
    prices: list


app = FastAPI()


@app.post("/")
async def root(request: PricesRequest):
    prices = np.array(list(map(float, request.prices)))
    result_prices = await mmar.get_result(prices)
    print(result_prices)
    return {"message"}


@app.post("/hurst")
async def root(prices: list = Body(...)):
    prices_numpy = np.array(list(map(float, prices)))
    h = await hurst_exponent.get_hurst_exponent_h_lib(prices_numpy)
    return h

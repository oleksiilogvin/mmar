import hurst
import numpy as np

from app import mmar


async def get_hurst_exponent_h_mmar(prices: np.ndarray) -> float:
    highly_composite_number = prices.size - 1
    price_log_deltas_xt = np.log(prices) - np.log(prices[0])
    time_deltas = mmar.get_time_deltas(highly_composite_number, prices)

    sample_sums = mmar.estimate_normalized_partition_function(time_deltas, price_log_deltas_xt)
    tau_q = mmar.estimate_scaling_function_tau_q(sample_sums, time_deltas, highly_composite_number)
    tau_q_polyfit = mmar.get_polyfit(tau_q)
    return mmar.get_hurst_exponent_h(tau_q_polyfit)


async def get_hurst_exponent_h_rs(prices: np.ndarray) -> float:
    lags = range(2, 20)

    tau = [np.std(np.subtract(prices[lag:], prices[:-lag])) for lag in lags]

    reg = np.polyfit(np.log(lags), np.log(tau), 1)

    return reg[0]


async def get_hurst_exponent_h_lib(prices: np.ndarray) -> float:
    H, c, data = hurst.compute_Hc(prices, kind='price', simplified=False)
    return H

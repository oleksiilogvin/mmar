import numpy as np
from unittest import IsolatedAsyncioTestCase

from app import hurst_exponent


class Test(IsolatedAsyncioTestCase):

 def getI(self, i: int):
  return i * 15 if i % 2 == 0 else i * 10

 def getIRandom(self, i: int):
  return 2 if i % 2 == 0 else 1

 async def test_functionality(self):
  prices = np.array([self.getI(i) for i in range(1, 1000)])
  prices2 = np.flip(prices)
  prices3 = np.array([i for i in range(1, 1000)])
  prices4 = np.array([self.getIRandom(i) for i in range(1, 1000)])
  concatenated = np.concatenate((prices, prices2))
  print(await hurst_exponent.get_hurst_exponent_h_lib(prices3))
